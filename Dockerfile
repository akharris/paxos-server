FROM node:8.10

ENV NODE_ENV production
ENV APP_DIR /opt/hub-indexer

WORKDIR $APP_DIR

COPY package.json $APP_DIR
COPY package-lock.json $APP_DIR

RUN npm install
COPY . $APP_DIR

EXPOSE 3030

CMD ["npm", "start"]
