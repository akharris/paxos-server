// setup DB connection
const config = require('./config')
const mongoose = require('mongoose');
mongoose.connect(config.mongo_uri, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
const db = mongoose.connection;

module.exports = db;
