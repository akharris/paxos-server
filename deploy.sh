# this file just describes some steps I took while setting up the ECS cluster and launching some containers into it
# some steps

# create the ECR registry (did it in the GUI)

# create security group for cluster
aws ec2 create-security-group --group-name {cluster-security-group} --description "{some description}"

# create the ECS cluster

# create the task definition
aws ecs register-task-definition --cli-input-json file://{task-definition-json-file}

# create the ELB with
# - rules that allow all internet traffic on port 80 (and 443)
# - its own security group

# create a new target group
# - health check to the application's health check endpoint

# authorize traffic from the ALB to the ECS cluster
# i.e. allow load balancer to talk to cluster (i.e. allow the load balancer to be the ingress)
aws ec2 authorize-security-group-ingress --group-name {cluster-security-group} --protocol tcp --port 1-65535 --source-group {load-balancer-security-group} 

# define an ECS service (this will actually run the task on the cluster)
aws ecs create-service --cli-input-json file://{service-ecs-json}
