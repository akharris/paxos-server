require('dotenv').config()

module.exports = Object.freeze({
  mongo_uri: process.env.MONGO_URI,
  port: process.env.PORT || 3030
})
