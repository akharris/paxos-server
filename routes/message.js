const express = require('express');
const router = express.Router();

const controller = require('../controllers/message');

router.post('/', controller.create);
router.get('/test', controller.test);
router.get('/:hash', controller.find);

module.exports = router;
