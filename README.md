# Paxos Code Challenge 1

Paxos API Sandbox can be found at [https://paxos-code-challenge.akharris.org](https://paxos-code-challenge.akharris.org).

Goal: Build a service that has the following two endpoints.

## Assumptions
- All requests are JSON

| Route | Description |
| ---   | -------     |
| POST /message | takes a message and returns the SHA256 hash digest |
| GET /message/:hash | returns the decrpyted message or a 404 (if no hash exists) |

## Brief Architecture Overview

I took this coding challenge as an excuse to check out Amazon ECS and Application Load Balancers (ALB for short). The
application is a simple Express.js app, running in Docker containers inside a small Amazon ECS cluster. This web services also
uses a little mlab-hosted MongoDB instance as the data store. The ECS runs inside its own security group by default, and that
security group allows the ALB (similar to an ELB) to forward traffic from the internet to the application containers.
I also added an SSL cert which terminates at the ALB level.

## Scaling Considerations

Questions: What would the bottleneck(s) be in your implementation as you acquire more users? How you might scale your
microservice?

I did very little to optimize the MongoDB queries so that might be the first issue, and that would be helped by indexing
properly. It might also help to get a larger instance size so the indexes that can be held in memory can be larger.

If the application runs into memory or CPU constraints we can adjust the instances that we provision for the microservice.
We can also scale the application horizontally using one or many metrics to determine application performance and load.

Beyond that the scaling strategies would vary based on the data consistency requirements. If the application has a high 
consistency guarantee then we're in the business of scaling DBs horizontally. One way of doing that is to use a leader &
replicator model, along with possibly sharding the DBs based on geographic location.

If, on the other hand, we have an **eventual consistency** guarantee then we could offload all of the DB writes into a
background job queueing system. Another optimization layer on top of that would batch the writes so we're making bulk inserts
instead of pummelling the DB once per message.

If we need to account for failure recovery scenarios then it would make sense to move to a hosted DB solution so we can rely
on automated snapshots using the built-in features of AWS/Azure/Google Cloud/IBM's Cloud Thing/whatever.

We could also add another layer of redundancy by using something like Amazon Kinesis as an event log and data
ingestion firehose. Kinesis has the ability to 'replay' data streams (source: [AWS Kinesis press release](https://aws.amazon.com/about-aws/whats-new/2016/04/amazon-kinesis-streams-introduces-time-based-shard-iterators-and-shard-level-metrics/)) which would
give the application another layer of redundancy. A Kineses replay ability plus frequent DB snapshots would allow the application
to recover from catastrophic events very quickly.
