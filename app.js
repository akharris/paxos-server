const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const messageRoute = require('./routes/message');
const app = express();
const config = require('./config')

// setup DB connection
db = require('./db');
db.on('error', console.error.bind(console, 'MongoDB connection error'));

// middleware
app.use(cors());
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// define routes
app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname + '/index.html'));
})
app.get('/robots.txt', function (req, res) {
  res.sendFile(path.join(__dirname + '/robots.txt'));
})
app.get('/swagger.json', function (req, res) {
  res.sendFile(path.join(__dirname + '/swagger.json'));
})
app.use('/messages', messageRoute);

const port = config.port;
app.listen(port, () => {
  console.log('server listening on port ' + port);
})
