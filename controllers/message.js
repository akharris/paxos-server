const Message = require('../models/message');
const crypto = require('crypto');

module.exports = {
  test: function (req, res) {
    res.json({ message: 'Greetings from the test endpoint' });
  },
  create: function (req, res, next) {
    const msg = req.body.message;
    const hash = crypto.createHash('sha256').update(msg).digest('hex');
    let message = new Message({message: msg, hash});
    message.save(err => {
      if (err) return next(err);
      res.json({ digest: hash });
    })
  },
  find: function (req, res, next) {
    const hash = req.params.hash;
    const message = Message.findOne({hash}, function (err, msg) {
      if (err) return next(err);
      if (msg && msg.message) {
        res.json({ message: msg.message });
      } else {
        res.status(404).json({err_msg: 'not found'});
      }
    })
  }
}
